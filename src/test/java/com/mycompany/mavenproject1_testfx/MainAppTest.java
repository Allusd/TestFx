/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.mavenproject1_testfx;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.*;
import static org.loadui.testfx.Assertions.assertNodeExists;
import static org.loadui.testfx.Assertions.verifyThat;
import static org.loadui.testfx.controls.Commons.hasText;
import org.loadui.testfx.utils.Matchers.*;

/**
 *
 * @author nikue
 */
public class MainAppTest extends GuiTest {
    
    public MainAppTest() {
    }
   
  @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = new MainApp.CounterPane();
            return parent;
        } catch (Exception ex) {
            System.out.println("poikkeus");
        }
        return parent;
    }
    /**
     * Test of start method, of class MainApp.
     * @throws java.lang.Exception
     */
    
    public void testStart() throws Exception {
        System.out.println("start");
        Stage stage = null;
        MainApp instance = new MainApp();
        instance.start(stage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class MainApp.
     */
     @Test
    public void should_initialize_countValue() {        
        // expect:
        verifyThat("#countValue", hasText("0"));
    }

    @Test
    public void should_increment_countValue() {      
        // when:
        
        click("count");
        
        // then:
        verifyThat("#countValue", hasText("5"));
    }

    @Test
    public void should_increment_countValue_again() {
        // when:
        click("count");

        // then:
        verifyThat("#countValue", hasText("5"));
    }
        @Test
        public void should_deincrement_countValue() {      
        // when:
        
        click("decount");
        
        // then:
        verifyThat("#countValue", hasText("-1"));
    }
            @Test
    public void setBothnamesAndCheckEnabledSearchButton() {
        verifyThat("#firstname", hasText("bennet"));
        verifyThat("#lastname", hasText("schulz"));

    }
    

}

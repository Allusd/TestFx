package com.mycompany.mavenproject1_testfx;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.geometry.Insets;



public class MainApp extends Application {
    @FXML
    private Button count; 
    private Button decount;
    private TextField firstname;
    private TextField lastname;
     
    @FXML
    private TextField countValue;
     
   public static class CounterPane extends StackPane {  
        public CounterPane() {
            super();
            TextField firstname = new TextField("firstname");
            TextField lastname = new TextField("lastname");
            firstname.setText("bennet");
            lastname.setText("schulz");
            lastname.setId("lastid");
            firstname.setId("firstId");
            
            // create countButton.
            Button countButton = new Button("count");
            countButton.setId("count");
            Button decountButton = new Button("decount");
            decountButton.setId("decount");

            // create countValue.
            TextField countValue = new TextField("0");
            countValue.setId("countValue");

            // initialize controls.
            countButton.setOnAction(event -> {
                String valueStr = countValue.getText();
                int incrementedValue = Integer.parseInt(valueStr) + 5;
                countValue.setText("" + incrementedValue);
            });
                        // initialize controls.
            decountButton.setOnAction(event -> {
                String valueStr = countValue.getText();
                int incrementedValue = Integer.parseInt(valueStr) - 1;
                countValue.setText("" + incrementedValue);
            });
            countValue.setEditable(false);
            countValue.setPrefWidth(50);

            // create and add containers.
            HBox box = new HBox(10, countButton, decountButton, countValue, lastname, firstname);
            box.setPadding(new Insets(10));
            box.setAlignment(Pos.CENTER);
            getChildren().add(box);
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = new CounterPane();

        Scene scene = new Scene(root,200,50);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

}
